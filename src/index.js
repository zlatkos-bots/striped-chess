require('dotenv').config();
const logger = require('./logger')('App');
const { init } = require('./db');

const listen = require('./comments');

const getClient = require('./client').getClient;

logger.info('Starting the app.');

(async () => {
  try {
    logger.info('Starting the database connection')
    const db = await init(process.env.DB_PATH);
    logger.info('db connected');
    const results = await db.getAllComments();
    logger.info(`${results.length} entries in db.`);
    const client = getClient();
    logger.info('Client created.');
    listen(client, db);
  } catch (e) {
    console.error('Error listening:', e);
    process.exit(1);
  }
})();

