const { CommentStream } = require('snoostorm');

const logger = require('./logger')('comments');

const holyHellPhrase= /^(\s*)holy hell(!*)(\s*)$/i;
const goodBotPhrase= /^(\s*)good bot([!?.]*)(\s*)$/i;

const isHellHoly = (comment) => {
  const { author: { name: user }, body } = comment;
  if (body.match(holyHellPhrase)) {
    logger.debug('it is a match!');
    const line = `${body.replace(/\n/, '\\n').substr(0, 140)}${body.length > 140 ? '...' : ''}`;
    logger.debug(`Comment by ${user}: ${line}`);
    return true;
  } else {
    // no match
    return false;
  }
}

const didTheyKnowItsBot = (comment) => {
  const { author: { name }, body } = comment;
  if (body.match(goodBotPhrase)) {
    logger.debug('They thanked us!');
    logger.debug(`Thanks by ${name}: ${body}`);
    console.log(comment);
    return true;
  } else {
    // sad face
    logger.debug('😔 They did not thank us');
    return false;
  }
}

const replyTo = (item) => {
  return item.reply('Haha chess is interesting! Now I am going to kill myself.')
    .then(response => {
      logger.debug('Replied', response);
      return response;
    },
    console.error);
}

function startListening(client, db) {
  let total = 0;
  let matches = 0;
  logger.debug('Starting listening on', client)

  const comments = new CommentStream(client, {
    subreddit: 'testingground4bots',
    limit: 5,
    pollTime: 2000
  });
  logger.debug('Comment stream set up.');

  comments.on('item', async (item) => {
    total++;
    if (isHellHoly(item)) {
      logger.debug('We have a string match', item.id, item.body);
      if (await db.isRepliedTo(item.id, item.link_id)) {
        logger.debug(`Item ${item.id} already in db, skip.`);
      } else {
        logger.debug('Marking item', item?.id)
        await db.markReplied(item.id, item.link_id);
        logger.debug('Marked item');
        /**
         * We will only reply to like 30% of items actually.
         */
        if (Math.random() > .3) {
          const reply = await replyTo(item);
          logger.debug('Reply response:', reply);
          logger.debug('To upodate');
          logger.debug({ itemId: item.id, replyId: reply.id, replyIsItem: item.id === reply.id });
          await db.updateMyReply(item.id, reply.id);
        } else {
          logger.debug('But we won\'t actually reply, just lock this guy out');
          await db.sayWeDidntSayIt(item.id);
        }
        matches++;
      }
    } else if (didTheyKnowItsBot(item)) {
      console.log('they know it\'s a bot. thanks, I hate it' + item.parent_id);
      if (await db.canIThankYou(item.link_id, item.id)) {
        logger.debug('yes we can');
        // const res = await db.thankEm(stuff we know we dont know)
        // await db.updateMyReply( find the ids);
      } else {
        logger.debug('no not our thing');
      }
    }

    logger.debug(`Status: ${matches} matches/${total} processed.`);
  });
}

module.exports = startListening;
