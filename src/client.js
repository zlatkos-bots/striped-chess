const Snoowrap = require('snoowrap');

const { userAgent } = require('./consts');

function getClient() {
  return new Snoowrap({
    userAgent,
    clientId: process.env.REDDIT_CLIENT_ID,
    clientSecret: process.env.REDDIT_CLIENT_SECRET,
    username: process.env.REDDIT_USER,
    password: process.env.REDDIT_PASS,
    continueAfterRatelimitError: true,
    scope: [
      'identity', 'edit', 'read', 'submit', 'subscribe', 'vote'
    ]
  });
}

module.exports = {
  getClient
};

