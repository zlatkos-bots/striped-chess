const pino = require('pino');

const defaults = {
  colorize: true,
  level: (process.env.LOG_LEVEL || 'debug').toLowerCase()
}

const createLogger = (name, options = {}) => pino({
  name,
  ...Object.assign({}, defaults, options)
});

module.exports = createLogger;
