const { open } = require('sqlite')
const sqlite3 = require('sqlite3');
const SQL = require('sql-template-strings');
const logger = require('./logger')('db');

const { maxPerThread, maxThankYous } = require('./consts');

let database;

const formatter = row => ({ ...row, repliedDate: new Date(row.repliedDate)});

const db = {
  /**
   * check is a comment has an entry
   */
  async isRepliedTo(commentId, threadId) {
    if (!commentId) {
      throw new Error('commentId is mandatory');
    }
    if (!threadId) {
      throw new Error('Thread Id is mandatory');
    }
    const existsQuery = SQL`
    SELECT
        COUNT(commentId) as commentCount
    FROM replies
    WHERE
      commentId=${commentId}
      AND threadId=${threadId}`;
    const threadCountQuery = SQL`
    SELECT
      COUNT(commentId) as threadCount
    FROM replies
    WHERE
        threadId = ${threadId}
    `;

    const { commentCount } = await database.get(existsQuery);
    const { threadCount } = await database.get(threadCountQuery);
    if (commentCount > 0 || threadCount > maxPerThread) {
      logger.debug('Response already in db:');
      logger.debug({ threadCount, commentCount });
      return true;
    } else {
      logger.debug('Not in db');
      logger.debug({ threadCount, commentCount, commentId, threadId });
    }
    return false;
  },
  /**
   * Insert a comment into db, basically locking it
   */
  markReplied(commentId, threadId, description = '') {
    if (!commentId) {
      throw new Error('No comment id');
    }
    if (!threadId) {
      throw new Error('Thread id is mandatory');
    }
    const date = new Date().toISOString();
    const query = SQL`
    INSERT INTO replies
      (commentId, threadId, repliedDate, desc)
      VALUES (${commentId}, ${threadId}, ${date}, ${description})
    `;
    return database.run(query);
  },
  /**
   * add my reply thread
   */
  updateMyReply(commentId, replyId) {
    const query = SQL`
    UPDATE replies
    SET
      replyId=${replyId}
    WHERE
      commentId=${commentId}
    `;
    logger.debug('Update my thread:')
    logger.debug(query);
    return database.run(query);
  },
  /**
   * Get all comments in the db, paged by 10 results
   */
  getAllComments(offset = 0) {
    const query = SQL`
    SELECT
      *
    FROM replies
    ORDER BY repliedDate
    LIMIT 10 OFFSET ${offset}
    `;
    return database.all(query)
      .then(replies => replies.map(formatter));
  },
  /**
   * We can only thank them if:
   * - they replied our comment
   * - and we didn't exceed the thread thankyous threshold
   */
  async canIThankYou(threadId, replyId) {
    const maxThanksQuery = SQL`
    SELECT
        count(commentId) as thankYouCount
    FROM replies
    WHERE
        threadId=${threadId}
        AND thankyou=TRUE
    `;
    const isOurReplyQuery = SQL`
    SELECT
      count(commentId) as isOurReply
    FROM replies
    WHERE
      replyId=${replyId}
    `;

    logger.debug(`Did we thanked at ${threadId}`);
    const { thankYouCount } = await database.get(maxThanksQuery);
    logger.debug(`And is ${replyId} our reply`);
    const { isOurReply } = await database.get(isOurReplyQuery);
    logger.debug('let us find out');
    if (thankYouCount >= maxThankYous) {
      logger.debug('Nope, too much');
      return false;
    }
    if (isOurReply !== 1) {
      logger.debug('not our comment they\'re replying to, who cares');
      return false;
    }
    logger.debug('Sure, thank\'em');
    return true;
  },
  /**
   * Mark that a particular reply is a "thank you" note
   */
  async thankEm(threadId, itemId) {
    const thankYouDate = new Date().toISOString();
    const query = SQL`
    INSERT INTO replies
      (commentId, threadId, repliedDate, thankyou)
      VALUES (${commentId}, ${threadId}, ${thankYouDate}, TRUE)
    `;
    logger.debug('Thanking them.');
    const res = await database.exec(query);
    logger.debug('Done.');
    logger.debug(res);
    return true;
  },
  /**
   * Sometimes we do not want to say it. We just noted that there was a holy hell, but we didn't actually respond.'
   */
  sayWeDidntSayIt(commentId) {
    const sayQuery = SQL`
    UPDATE replies
    SET
      iDidntDoIt=TRUE
    WHERE
      commentId=${commentId}
    `;
    logger.debug('we will say we did not comment.');
    return database.run(sayQuery);
  }
};

const createTableIfNotExists = async () => {
  await database.exec(`
  CREATE TABLE IF NOT EXISTS replies (
    commentId TEXT PRIMARY KEY,
    threadId TEXT NOT NULL,
    replyId TEXT default "",
    repliedDate TEXT NOT_NULL,
    thankyou INTEGER default 0
  ) WITHOUT ROWID
  `);
}

const init = async (dbPath) => {
  logger.debug(`Opening the database at ${dbPath}...`);
  database = await open({
    filename: dbPath,
    driver: sqlite3.Database
  });
  await  createTableIfNotExists();
  return db;
}

module.exports = {
  init,
}
