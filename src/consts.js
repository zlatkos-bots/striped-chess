const userAgent = 'zlatko.dev/zlatkos-bots/striped-chess:v0.1.0';
const maxPerThread = 5;
const maxThankYous = 3;

module.exports = {
  userAgent,
  maxPerThread,
  maxThankYous,
}

